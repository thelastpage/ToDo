package org.hackerearth.maven.todo.web.bean;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TaskMapper implements RowMapper {
	   public Tasks mapRow(ResultSet rs, int rowNum) throws SQLException {
		      Tasks task = new Tasks();
		      task.setTaskId(rs.getInt("task_id"));
		      task.setTaskName(rs.getString("task_name"));
		      task.setTaskDescription(rs.getString("task_description"));
		      task.setTaskPriority(rs.getString("task_priority"));
		      task.setTaskStatus(rs.getString("task_status"));
		      task.setStartTime(rs.getTimestamp("task_start_time"));
		      task.setEndTime(rs.getTimestamp("task_end_time"));
		      task.setTaskArchived(rs.getBoolean("task_archived"));
		      
		      return task;
		   }
}
