package org.hackerearth.maven.todo.web.controller;

import java.text.ParseException;
import java.util.List;

import org.hackerearth.maven.todo.web.bean.Tasks;
import org.hackerearth.maven.todo.web.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {
	
	@Autowired
	ToDoService service;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		service.getTasks();
		return "index";
	}
	
	@RequestMapping(value="/load",method = RequestMethod.GET,headers="Accept=application/json")
	@ResponseBody
	public List<Tasks> getTasks() {
		System.out.println("Inside Controller");
		List<Tasks> tasks= service.getTasks();
		return tasks;
	}
	
	 @RequestMapping(value="/insert/{taskName}/{taskDesc}/{taskPriority}/{taskStatus}",method = RequestMethod.POST,headers="Accept=application/json")
	 @ResponseBody
	 public List<Tasks> addTask(@PathVariable String taskName,@PathVariable String taskDesc,@PathVariable String taskPriority,@PathVariable String taskStatus) throws ParseException {	
		try {
			Tasks task = new Tasks();
			task.setTaskName(taskName);
			task.setTaskDescription(taskDesc);
			task.setTaskPriority(taskPriority);
			task.setTaskStatus(taskStatus);
			boolean status=service.addTask(task);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return service.getTasks();
		 
	 }	 
	 
	 @RequestMapping(value="/search/{taskId}",method = RequestMethod.POST,headers="Accept=application/json")
	 @ResponseBody
	 public Tasks searchTask(@PathVariable String taskId) throws ParseException {	
		 	Tasks task=new Tasks();
			try {
				task= service.searchTask(taskId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
			return task;
	 }	 
	 
	 @RequestMapping(value="/update/{taskName}/{taskDesc}/{taskPriority}/{taskStatus}/{taskId}",method = RequestMethod.POST,headers="Accept=application/json")
	 @ResponseBody
	 public List<Tasks> updateTask(@PathVariable String taskName,@PathVariable String taskDesc,@PathVariable String taskPriority,@PathVariable String taskStatus,@PathVariable String taskId) throws ParseException {	
		try {
			Tasks task = new Tasks();
			task.setTaskName(taskName);
			task.setTaskDescription(taskDesc);
			task.setTaskPriority(taskPriority);
			task.setTaskStatus(taskStatus);
			task.setTaskId(Integer.parseInt(taskId));
			boolean status=service.update(task);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return service.getTasks();
		 
	 }	 
	 
	 @RequestMapping(value="/archive/{taskId}",method = RequestMethod.POST,headers="Accept=application/json")
	 @ResponseBody
	 public List<Tasks> deleteTask(@PathVariable String taskId) throws ParseException {	
		try {
			Tasks task = new Tasks();			
			task.setTaskId(Integer.parseInt(taskId));
			boolean status=service.delete(task);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return service.getTasks();
		 
	 }	 
}