package org.hackerearth.maven.todo.web.dao;

import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;

import org.hackerearth.maven.todo.web.bean.TaskMapper;
import org.hackerearth.maven.todo.web.bean.Tasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;

@Repository
public class ToDoDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;  
	  
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {  
	    this.jdbcTemplate = jdbcTemplate;  
	}  
	
	@SuppressWarnings("unchecked")
	public List <Tasks> getTasks(){
		ArrayList<Tasks> task=new ArrayList<Tasks>();
		try {
			String sql="select * from task_list";  
			task = (ArrayList<Tasks>) jdbcTemplate.query(sql, new TaskMapper());	
			for(Tasks t:task){
				System.out.println(t.getTaskDescription());
			}
		} 
		catch(Exception e){
			e.printStackTrace();
		}		
		
		return task; 
	}

	public Boolean addTasks(final Tasks task) {
		String query="insert into task_list(task_name,task_description,task_priority,task_status,task_archived,task_start_time,task_end_time) values (?, ?, ?,?,?,?,?)";
		try {
			DataSource ds=jdbcTemplate.getDataSource();
			Connection connection=ds.getConnection();
			PreparedStatement preparedStatement = connection
				     .prepareStatement(query);
				  
		   preparedStatement.setString(1, task.getTaskName());
		   preparedStatement.setString(2, task.getTaskDescription());   
		   preparedStatement.setString(3, task.getTaskPriority());
		   preparedStatement.setString(4, task.getTaskStatus());
		   preparedStatement.setBoolean(5, false);
		   Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		   preparedStatement.setTimestamp(6,currentTime);
		   preparedStatement.setTimestamp(7,currentTime);
		   preparedStatement.executeUpdate();
				    				    	  
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
		
	}

	@SuppressWarnings("unchecked")
	public Tasks searchTask(int taskId) {
		ArrayList<Tasks> task=new ArrayList<Tasks>();
		try {
			String sql="select * from task_list where task_id="+taskId;  
			task = (ArrayList<Tasks>) jdbcTemplate.query(sql, new TaskMapper());
			
		} catch (DataAccessException e) {			
			e.printStackTrace();
		}
		if(task.size()==0)
			return new Tasks();
		else
			return task.get(0);
	}

	public boolean updateTask(Tasks task) {
		String sql= "UPDATE task_list SET task_name=\'"+task.getTaskName()+"\',task_description=\'"+task.getTaskDescription()+
					"\',task_priority=\'"+task.getTaskPriority()+"\',task_status=\'"+task.getTaskStatus()+"\' where task_id="+task.getTaskId();
		try {
			jdbcTemplate.update(sql);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
	}

	public boolean deleteTask(Tasks task) {
		String sql= "Delete from task_list where task_id="+task.getTaskId();
		try {
			jdbcTemplate.update(sql);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}
}
