package org.hackerearth.maven.todo.web.service;

import java.util.List;

import org.hackerearth.maven.todo.web.bean.Tasks;
import org.hackerearth.maven.todo.web.dao.ToDoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ToDoService {
	
	@Autowired
	ToDoDao dao;
	
	public List<Tasks> getTasks(){
		return dao.getTasks();
	}

	public Boolean addTask(Tasks task) {
		return dao.addTasks(task);
		
	}

	public Tasks searchTask(String taskId) {
		// TODO Auto-generated method stub
		return dao.searchTask(Integer.parseInt(taskId));
	}

	public boolean update(Tasks task) {
		// TODO Auto-generated method stub
		return dao.updateTask(task);
	}

	public boolean delete(Tasks task) {
		// TODO Auto-generated method stub
		return dao.deleteTask(task);
	}
}
