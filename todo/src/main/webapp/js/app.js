var taskManagerModule = angular.module('taskManagerApp', ['ngAnimate']);

taskManagerModule.controller('taskManagerController', function ($scope,$http) {
	
	var urlBase="http://localhost:8080/todo";
	$scope.taskFlag=false;
	$scope.queryFlag=true;
	$scope.selection = [];
	$scope.statuses=['NEW','ACTIVE','COMPLETED'];
	$scope.priorities=['HIGH','LOW','MEDIUM'];
	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
	$scope.input = {};
	$scope.tasks={};	
	
	//get all tasks and display initially
	$http.get(urlBase+'/load').
    	then(function(success) {
	        $scope.tasks = success.data;
	        
	        for(var i=0;i<$scope.tasks.length;i++){
	           	 $scope.selection.push($scope.tasks[i].id);
	        	}
	        },function(error) {
	    console.log(error.data);
	});
	
	//add a new task
	$scope.addTask = function addTask() {
		if($scope.taskName=="" || $scope.taskDesc=="" || $scope.taskPriority == "" || $scope.taskStatus == ""||$scope.taskName==undefined || $scope.taskDesc==undefined || $scope.taskPriority == undefined || $scope.taskStatus == undefined){
			alert("Insufficient Data! Please provide values for task name, description, priortiy and status");
		}
		else{
		 $http.post(urlBase+'/insert/' +$scope.taskName+'/'+$scope.taskDesc+'/'+$scope.taskPriority+'/'+$scope.taskStatus).
		  then(function(success){
			 alert("Task added");
			 $scope.tasks = success.data;	
			 $scope.taskName="";
			 $scope.taskDesc="";
			 $scope.taskPriority="";
			 $scope.taskStatus="";			 
		    },function(error) {
			    console.log(error.data);
			});
		}
	};
	
	//search a task
	$scope.searchTask = function searchTask() {
		
		if($scope.input.taskId=="" ||$scope.input.taskId== undefined || $scope.input.taskId==null){
			alert("Insufficient Data! Please provide value for task Id");
		}
		else{
		 $http.post(urlBase+'/search/' +$scope.input.taskId).
		  then(function(success){
			 if(success.data.taskName=="" || success.data.taskName==null ){
				 alert("Invalid Task Id");
				 $scope.taskFlag=false;
				 $scope.queryFlag=true;
			 }
			 else{
				 $scope.taskData = success.data;
				 $scope.taskFlag=true;
				 $scope.queryFlag=false;
			 }
		    },function(error) {
			    console.log(error.data);
			    $scope.taskFlag=false;
			    $scope.queryFlag=true;
			});
		}
	};
	
	//add a new task
	$scope.updateTask = function updateTask() {
		if($scope.taskData.taskName=="" || $scope.taskData.taskDesc=="" || $scope.taskData.taskPriority == "" || $scope.taskData.taskStatus == ""){
			alert("Insufficient Data! Please provide values for task name, description, priortiy and status");
		}
		else{
		 $http.post(urlBase+'/update/' +$scope.taskData.taskName+'/'+$scope.taskData.taskDescription+'/'+$scope.taskData.taskPriority+'/'+$scope.taskData.taskStatus+'/'+$scope.input.taskId).
		  then(function(success){
			 alert("Task updated");
			 $scope.tasks = success.data;	 
		    },function(error) {
			    console.log(error.data);
			});
		}
	};
	
	// Archive Completed Tasks
	  $scope.archiveTasks = function archiveTasks(Id) {
		  $http.post(urlBase+'/archive/' + Id).
		  then(function(success){
				 alert("Task Deleted");
				 $scope.tasks = success.data;	 
			    },function(error) {
				    console.log(error.data);
				});
			};
	  
	  $scope.changeId=function changeId(Id){
		  $scope.input.taskId=Id;
		  jQuery("#changePath").click();
		  $scope.searchTask();
	  };
	  
	  $scope.displaySearch=function displaySearch(){
		  $scope.taskFlag=false;
		  $scope.queryFlag=true;
	  };
});

